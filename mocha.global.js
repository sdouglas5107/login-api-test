const app = require('./');
const mongoose = require('mongoose');

after(function (done) {
  app.server.on('close', () => done());
  mongoose.connection.close();
  app.server.close();
});
