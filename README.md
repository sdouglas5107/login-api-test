## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 6.x.x
- [Gulp](http://gulpjs.com/)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server.

## Build 

Run `gulp build` for building.

## Testing

Running `npm test` will run the unit and integration tests with mocha.

## Having a try
* For creating a account, do a `POST` request for `/api/users` pasing the body:
  ```
  {
    "name":"Your Name",
    "email":"email@test.com",
    "password":"123456"
  }
  ```
* For login, do a `POST` request for `/auth/local` passing the body:
  ```
  {
    "email":"ds@ds.ds",
    "password":"123456"
  } 
  ```
