/**
 * Main application routes
 */

'use strict';

module.exports = function(app) {
  // Insert routes below
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.json('Login Api Lives');
    });
};
