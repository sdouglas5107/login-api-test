'use strict';

/* globals describe, expect, it, before, after*/

const app = require('../..');
const User = require('./user.model');
const request = require('supertest');

describe('User API:', function () {
  let user;

  // Clear users before testing
  before(function () {
    return User.remove().then(function () {
      user = new User({
        name: 'Fake User',
        email: 'test@example.com',
        password: 'password',
        role: 'admin'
      });

      return user.save();
    });
  });

  // Clear users after testing
  after(function () {
    return User.remove();
  });

  describe('GET /api/users', function () {
    var token;

    before(function (done) {
      request(app)
        .post('/auth/local')
        .send({
          email: 'test@example.com',
          password: 'password'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          token = res.body.token;
          done();
        });
    });

    it('should respond with a list of users when authenticated and is admin', function (done) {
      request(app)
        .get('/api/users')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          expect(res.body).to.be.an('array');
          done();
        });
    });

    it('should respond with a 401 when not authenticated', function (done) {
      request(app)
        .get('/api/users')
        .expect(401)
        .end(done);
    });
  });
});
